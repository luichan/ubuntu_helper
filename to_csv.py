import csv
import os

with open('old_top.csv', "w") as csv_fp:
    writer = csv.writer(csv_fp)
    for line in open('old_top.txt'):
        row = ','.join(line.split())
        csv_fp.write(row+'\n')

with open('new_top.csv', "w") as csv_fp:
    writer = csv.writer(csv_fp)
    for line in open('new_top.txt'):
        row = ','.join(line.split())
        csv_fp.write(row+'\n')
print("Done")
