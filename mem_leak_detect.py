import os
import csv
from collections import OrderedDict

COL_PID = "PID"
COL_USER = "USER"
COL_VIRT = "VIRT"
COL_RES = "RES"
COL_SHR = "SHR"
COL_CPU = "%CPU"
COL_MEM = "%MEM"
COL_COMMAND = "COMMAND"

COL_NEW = "NEW"
"""
Assume csv is from processed (adding comman) output from top -b -n 1 -o PID
"""
def get_dict_from_csv(filename):
    result = OrderedDict()
    with open(filename, 'r') as fp:
        old_csv = csv.DictReader(fp)
        for row in old_csv:
            pid_dict = {}
            pid = int(row.get(COL_PID))
            pid_dict[COL_USER] = row.get(COL_USER)
            pid_dict[COL_VIRT] = int(row.get(COL_VIRT))
            pid_dict[COL_RES] = int(row.get(COL_RES))
            pid_dict[COL_SHR] = int(row.get(COL_SHR))
            pid_dict[COL_CPU] = float(row.get(COL_CPU))
            pid_dict[COL_MEM] = float(row.get(COL_MEM))
            pid_dict[COL_COMMAND] = row.get(COL_COMMAND)
            result[pid] = pid_dict
    return result

def write_dict_to_csv(filename, a_list):
    with open(filename, 'w') as fp:
        fieldnames = [COL_NEW, COL_PID, COL_USER, COL_VIRT, COL_RES, COL_SHR, COL_CPU, COL_MEM, COL_COMMAND]
        writer = csv.DictWriter(fp, fieldnames=fieldnames)
        writer.writeheader()
        for row in a_list:
            writer.writerow(row)

old_dict = get_dict_from_csv('old_top.csv')
new_dict = get_dict_from_csv('new_top.csv')

diff_list = []
for pid in new_dict:
    temp = {}
    temp[COL_PID]=pid
    if pid in old_dict:
        temp[COL_NEW]="Old"
        temp[COL_USER]=new_dict[pid][COL_USER]
        temp[COL_VIRT]=new_dict[pid][COL_VIRT] - old_dict[pid][COL_VIRT]
        temp[COL_RES]=new_dict[pid][COL_RES] - old_dict[pid][COL_RES]
        temp[COL_SHR]=new_dict[pid][COL_SHR] - old_dict[pid][COL_SHR]
        temp[COL_CPU]=new_dict[pid][COL_CPU] - old_dict[pid][COL_CPU]
        temp[COL_MEM]=new_dict[pid][COL_MEM] - old_dict[pid][COL_MEM]
        temp[COL_COMMAND]=new_dict[pid][COL_COMMAND]
    else:
        temp[COL_NEW]="New"
        temp[COL_USER]=new_dict[pid][COL_USER]
        temp[COL_VIRT]=new_dict[pid][COL_VIRT]
        temp[COL_RES]=new_dict[pid][COL_RES]
        temp[COL_SHR]=new_dict[pid][COL_SHR]
        temp[COL_CPU]=new_dict[pid][COL_CPU]
        temp[COL_MEM]=new_dict[pid][COL_MEM]
        temp[COL_COMMAND]=new_dict[pid][COL_COMMAND]
    diff_list.append(temp)

for pid in old_dict:
    if pid not in new_dict:
        temp = {}
        temp[COL_PID] = pid
        temp[COL_NEW]="Del"
        temp[COL_USER] = old_dict[pid][COL_USER]
        temp[COL_VIRT] = 0 - old_dict[pid][COL_VIRT]
        temp[COL_RES]= 0 - old_dict[pid][COL_RES]
        temp[COL_SHR] = 0 - old_dict[pid][COL_SHR]
        temp[COL_CPU] = 0 - old_dict[pid][COL_CPU]
        temp[COL_MEM] = 0 - old_dict[pid][COL_MEM]
        temp[COL_COMMAND] = old_dict[pid][COL_COMMAND]
        diff_list.append(temp)

write_dict_to_csv('diff_top.csv', diff_list)
print("Done")